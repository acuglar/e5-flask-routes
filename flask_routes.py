from flask import Flask
from datetime import date, datetime

app = Flask(__name__)

@app.route('/')
def home():
    return {'data': 'Hello Flask!'}

@app.route('/current_datetime')
def current_datetime():
    today = date.today().strftime('%d/%m/%Y')
    time = datetime.now().strftime('%H:%M:%S')
    ampm = 'AM' if '00:00:00' <= time < '12:00:00' else 'PM'
    message = ''
    
    if '00:00:00' <= time < '12:00:00':
        message = 'Bom dia!'
    elif '12:00:00' <= time < '18:00:00':
        message = 'Boa tarde!'
    else:
        message = 'Boa noite!'

    return {
        'current_datetime': f'{today} {time} {ampm}',
        'message': message
        }